## Plannings projections et caisse pour un cinéma itinérant.
* Planning pour organiser les séances de bénévoles (projectionnistes, caisse) dans un cinéma itinérant d'Arts et d'Essais avec plusieurs lieux de diffusion éditable en Ruby on Rails.
* Le but en de faire un outil de plannification à travers un agenda avec des tâches configurable et éditable pour les utilisateurs.
* Sentez-vous libre d'adapter ce projet si il vous intéresse.

## Plannings for a mobile cinema with voluntary workers.
* This application have been developed for an mobile cinema to organise the voluntary workers for the showings and the cashier for each shows. Since we've got numeric projectors, the activity have consequently increase and we needed a tool to make easier the task of the plannings.
* Actually, the application is on production [here](https://plannings-cinema.herokuapp.com/) on an Heroku's server because it's a simple way to deploy an Ruby on Rails application.
As it's also deployed on another server for the management of our Plannings, this adress o Heroku is now a demo application. You can be free to try it if you want. To try it, you can login [here](https://plannings-cinema.herokuapp.com/log_in) and to get connected, put this email : manager@demo.com and this password : manager to be connected as a 'manager' like the voluntary workers or with this email : admin@demo.com and this password : admin to be connected as an administrator. Be respectful, I open this application by my own risk to let interested people discover how it works.

## Installation Ruby on Rails with rvm and dependencies :
* Ruby version : ruby 2.7.2

* You need to install ruby and ruby on rails or rvm is a better solution.
To get rvm add some dependencies (on a GNU/linux OS):

```bash
$ sudo apt-get install -y curl gnupg build-essential gnupg2
$ sudo apt install gawk, autoconf, automake, bison, libffi-dev, libgdbm-dev, libncurses5-dev, libsqlite3-dev, libtool, libyaml-dev, pkg-config, sqlite3, zlib1g-dev, libgmp-dev, libreadline-dev, libssl-dev

$ gpg2 --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2.69D6956105BD0E739499BDB
$ \curl -sSL https://get.rvm.io | bash -s stable
$ source ~/.rvm/scripts/rvm
```

Have a look on this link to do it (don't forget to have a look if you already have an ruby version installed on your system, in that case you will need to remove it before !) : see [rvm](https://rvm.io/rvm/install).

* To get ruby 2.7.2 :

```bash
$ rvm install 2.7.2
```

* Then to get rails only add this on your bash (this will install a newest stable version of rails, for this project you need another version, see later) :

```bash
$ gem install bundler rails
```
 and

```bash
$ sudo apt-get install libpq-dev
```
## Nodejs et Yarn

You need also these dependencies for Rails

```bash
$ curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
$ sudo apt-get install -y nodejs` to install Node.js 10.x and npm
```
## You may also need development tools to build native addons:

```bash
$ sudo apt-get install gcc g++ make
```
## To install the Yarn package manager, run:

```bash
$ curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
$ echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
$ sudo apt-get update && sudo apt-get install yarn
```

## Installation:

* To configure the application, you can clone it, run

```bash
$ git clone git@framagit.org:rodinux/plannings_cinema.git
```
or fork it , as you want.

* To use ruby 2.7.2, don't forget doing this in your project :

```bash
$ cd plannings_cinema
$ rvm use 2.7.2
$ gem install rails
```

* Then run

```bash
$ bundle install
```

* IMPORTANT: You need also to create credentials 

### Credentials

If you already have a file config/credentials.yml.enc, first delete it

```bash
$ rm  config/credentials.yml.enc
```

and now create credentials

```bash
$ EDITOR="mate --wait" rails secrets:edit
```

## Deployment instructions

* It is possible you want to change the database, (I personnally use postgresql), if you want to use it without Postgresql and without creating first a database, it is very easy. You need to make few changes, in the file config/database.yml, replace all by :

## config/database.yml

```
# SQLite version 3.x
#   gem install sqlite3-ruby (not necessary on OS X Leopard)
development:
  adapter: sqlite3
  database: db/development.sqlite3
  pool: 5
  timeout: 5000

# Warning: The database defined as "test" will be erased and
# re-generated from your development database when you run "rake".
# Do not set this db to the same as development or production.
test:
  adapter: sqlite3
  database: db/test.sqlite3
  pool: 5
  timeout: 5000

production:
  adapter: sqlite3
  database: db/production.sqlite3
  pool: 5
  timeout: 5000
```

* You always need to migrate the database before running :

```bash
$ rake db:create
$ rake db:migrate
```

* To sart the application locally :

```bash
$ rails s
```

and go to localhost:3000 in your navigator
That's all :)

##Hidden ENVs with Figaro

The gem Figaro create a file config/application.yml running

```bash
$ bundle exec figaro install
```

This file is added in the .gitignore so you can edit this file to hide some variables like the configuration for send mails (I have configured a gmail address to send mails in config/environments.production.rb and config/environments/development.rb) or the user and password for a Postgresql database.

```
development:
GMAIL_USERNAME: 'mygmailadress@gmail.com'
GMAIL_PASSWORD: 'mypassword'
PG_USERNAME: 'my_pg_username'
PG_PASSWORD: 'my_pg_password'
```

## IDs for Villages

There is some files where I need change the IDs in this application when starting.
In the file views/films/index.html.erb, I need correct the values for the IDs for these lines because the IDs of the villages can change when I import them with Rails_admin_import. Found first the correct value for the Villages: Vernoux, Lamastre and Chalencon.

To understand what's going on, we are a cinema with twoo agrements, one for Vernoux which is a permanent cinema, Lamastre and Chalencon are twoo others cinema with a weekly programmation, and we do also itinerants projections in few villages. We need see the frequentation rate of each cinemas. The "Tournée" represent all the projections done within the Vernoux's projections.

```
 <% @films.order(updated_at: :desc).each do |film| %>
     <% film_seances_semaine = film.seances.where(annulee: [nil, ""]) %>
      <% film_vernoux= film_seances_semaine.where(village_id: 4) %>
      <% film_lamastre = film_seances_semaine.where(village_id: 8) %>
      <% film_chalencon = film_seances_semaine.where(village_id: 5) %>
```

Also in the file view/seances/entrees.hml.erb, need the correct IDs for Vernoux, Lamastre and Chalencon

```
[............]
      <tr>
          <td class="active">Vernoux</td>
          <td class="warning"><strong><%= @stats.seances_film_per_lieu(4).length %></strong> séance(s)</td>
          <td class="info"><strong><%= @stats.seances_film_per_lieu(4).sum("billets_scolaires") %></strong> scolaires</td>
          <td class="info"><strong><%= @stats.seances_film_per_lieu(4).sum("billets_enfants") %></strong> enfant(s)</td>
          <td class="info"><strong><%= @stats.seances_film_per_lieu(4).sum("billets_adultes") %></strong> adulte(s)</td>
          <td class="warning"><strong><%= @stats.seances_film_per_lieu(4).sum("total_billets") %></strong> entrée(s)</td>
      </tr>
      <tr>
          <td class="active">Lamastre</td>
          <td class="warning"><strong><%= @stats.seances_film_per_lieu(8).length %></strong> séance(s)</td>
          <td class="info"><strong><%= @stats.seances_film_per_lieu(8).sum("billets_scolaires") %></strong>  scolaires</td>
          <td class="info"><strong><%= @stats.seances_film_per_lieu(8).sum("billets_enfants") %></strong> enfant(s)</td>
          <td class="info"><strong><%= @stats.seances_film_per_lieu(8).sum("billets_adultes") %></strong> adulte(s)</td>
          <td class="warning"><strong><%= @stats.seances_film_per_lieu(8).sum("total_billets") %></strong> entrée(s)</td>
      </tr>
      <tr>
          <td class="active">Chalencon</td>
          <td class="warning"><strong><%= @stats.seances_film_per_lieu(5).length %></strong> séance(s)</td>
          <td class="info"><strong><%= @stats.seances_film_per_lieu(5).sum("billets_scolaires") %></strong>  scolaires</td>
          <td class="info"><strong><%= @stats.seances_film_per_lieu(5).sum("billets_enfants") %></strong> enfant(s)</td>
          <td class="info"><strong><%= @stats.seances_film_per_lieu(5).sum("billets_adultes") %></strong> adulte(s)</td>
          <td class="warning"><strong><%= @stats.seances_film_per_lieu(5).sum("total_billets") %></strong> entrée(s)</td>
      </tr>
      <tr>
          <td class="active">Itinérance</td>
          <td class="warning"><strong><%= @stats.seances_range.length - (@stats.seances_film_per_lieu(4).length + @stats.seances_film_per_lieu(8).length + @stats.seances_film_per_lieu(5).length) %></strong> séance(s)</td>
          <td class="info"><strong><%= @stats.seances_range.sum("billets_scolaires") - (@stats.seances_film_per_lieu(4).sum("billets_scolaires")  + @stats.seances_film_per_lieu(8).sum("billets_scolaires") + @stats.seances_film_per_lieu(5).sum("billets_scolaires")) %></strong> scolaires</td>
          <td class="info"><strong><%= @stats.seances_range.sum("billets_enfants") - (@stats.seances_film_per_lieu(4).sum("billets_enfants") + @stats.seances_film_per_lieu(8).sum("billets_enfants") + @stats.seances_film_per_lieu(5).sum("billets_enfants")) %></strong> enfant(s)</td>
          <td class="info"><strong><%= @stats.seances_range.sum("billets_adultes") - (@stats.seances_film_per_lieu(4).sum("billets_adultes") + @stats.seances_film_per_lieu(8).sum("billets_adultes") + @stats.seances_film_per_lieu(5).sum("billets_adultes")) %></strong> adulte(s)</td>
          <td class="warning"><strong><%= @stats.seances_range.sum("total_billets") - (@stats.seances_film_per_lieu(4).sum("total_billets") + @stats.seances_film_per_lieu(8).sum("total_billets") + @stats.seances_film_per_lieu(5).sum("total_billets")) %></strong> entrée(s)</td>
      </tr>
      <tr>
          <td class="active">Tournée (Lamastre, Chalencon, Itinérance)</td>
          <td class="warning"><strong><%= @stats.seances_range.length - @stats.seances_film_per_lieu(4).length %></strong> séance(s)</td>
          <td class="info"><strong><%= @stats.seances_range.sum("billets_scolaires") - @stats.seances_film_per_lieu(4).sum("billets_scolaires") %></strong> scolaires</td>
          <td class="info"><strong><%= @stats.seances_range.sum("billets_enfants") - @stats.seances_film_per_lieu(4).sum("billets_enfants") %></strong> enfant(s)</td>
          <td class="info"><strong><%= @stats.seances_range.sum("billets_adultes") - @stats.seances_film_per_lieu(4).sum("billets_adultes") %></strong> adultes</td>
          <td class="warning"><strong><%= @stats.seances_range.sum("total_billets") - @stats.seances_film_per_lieu(4).sum("total_billets") %></strong> entrée(s)</td>
      </tr>
    </tbody>
  </table>
</div>

    <center><h1><strong>Par Film et ordonné par Total des entrées</strong></h1></center>
<div class="container table-croped">
      <% @stats.films_order_total.each do |film| %>
      <% if film.seances.where(horaire: date_range).present? %>
       <% film_seances_semaine = film.seances.where(horaire: date_range, annulee: [nil, ""]) %>
      <% film_vernoux= film_seances_semaine.where(village_id: 4) %>
      <% film_lamastre = film_seances_semaine.where(village_id: 8) %>
      <% film_chalencon = film_seances_semaine.where(village_id: 5) %>
[..................]
```

## More Information

* You can found this application on [Framagit](https://git.framasoft.org/rodinux/plannings_cinema) or on [Github](https://github.com/rodinux/plannings_cinema).
I have added a [Wiki](https://git.framasoft.org/rodinux/plannings_cinema/wikis/home) on Framagit to get documentation if you want to adapt this project.

## Deploying on heroku

* In heroku you need to keep the file config/database.yml configured with pgsql and to have the gem 'pg' on your file Gemfile.

* To get ruby 2.7.2 :

```bash
$ rvm install 2.7.2
```

* To use ruby 2.7.2 don't forget doing this in your project :

```bash
$ cd plannings_cinema
$ rvm use 2.7.2
$ gem install rails
```
Then run

```bash
$ bundle install
```

* to use wicked_pdf on heroku you need to change few things, add the gem wkhtmltopdf-heroku in the group production

```
group :production do
  gem 'pg', group: :production
  gem 'rails_12factor', group: :production
  gem 'wkhtmltopdf-heroku', group: :production
end
```

* Don't put anything in config/initializers/mimes_types.rb or just comment it

* also in config/environnements/production.rb, comment or erase this line :

```
config.after_initialize do
    WICKED_PDF[:exe_path] = "/usr/bin/wkhtmltopdf.sh"
  end
```

* in config/initialers/wicked_pdf.rb

```
if Rails.env.staging? || Rails.env.production?
  exe_path = Rails.root.join('bin', 'wkhtmltopdf-amd64').to_s
else
  exe_path = Rails.root.join('bin', 'wkhtmltopdf').to_s
end
```

* Have a look at : [Wicked_pdf Configuration](https://github.com/mileszs/wicked_pdf/wiki/Configuration) and [Wicked_pdf RubyDoc](http://www.rubydoc.info/gems/wicked_pdf/0.7.2)

Have fun, <3 Ruby on Rails


