class ApplicationMailer < ActionMailer::Base
  default from: ENV["MAIL_USERNAME"]

  # def bienvenue_email(user)
  #   @user = user
  #   mail(to: @user.email, subject: 'Bienvenue sur le planning des séances Écran Village')
  # end

end
