class AddImportIdToVillages < ActiveRecord::Migration[6.0]
  def change
    add_column :villages, :import_id, :integer
  end
end
