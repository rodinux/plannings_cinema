json.array!(@seances.seances_a_venir) do |seance|
    json.extract! seance, :id, :film_id, :version, :audio_description, :village_id, :extras, :annulee
    json.horaire(seance.horaire.strftime('%s'))
end
