Rails.application.routes.draw do
	mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
	mount RailsAdminImport::Engine => '/rails_admin_import', :as => 'rails_admin_import'
	root to: "calendar#calendrier", :lieu => "tous les lieux"
	get 'log_in' => 'user_sessions#new', :as => :log_in
	post 'log_out' => 'user_sessions#destroy', :as => :log_out

  resources :reset_passwords, only: [:new, :create, :update, :edit]
	resource :calendar, only: [:calendrier, :aide], controller: :calendar
	resource :iframe, only: [:calendrier_iframe], controller: :iframe
	resource :specific_iframe, only: [:specific_calendrier_iframe], controller: :specific_iframe
	resources :ics
	resources :seances
	resources :villages
	resources :films
	resources :classifications
	resources :users
	resources :user_sessions, only: [:new, :create, :destroy]
	resources :disponibilites
	get 'specific_calendrier_iframe' => 'specific_iframe#specific_calendrier_iframe'
	get 'calendrier_iframe' => 'iframe#calendrier_iframe'
	get 'aide' => 'calendar#aide'
	get 'calendrier' => 'calendar#calendrier'
	get 'a_completer' => 'seances#a_completer'
	get 'mes_seances' => 'seances#mes_seances'
	get 'toutes_mes_seances' => 'seances#toutes_mes_seances'
	get 'seances_passees' => 'seances#seances_passees'
	get 'edition_calendrier' => 'seances#edition_calendrier'
	get 'entrees' => 'seances#entrees'
	get 'export_csv' => 'seances#export_csv'
	get 'export_nodon_csv' => 'seances#export_nodon_csv'
	get 'export_cc_csv' => 'seances#export_cc_csv'
	get 'export_tournee_csv' => 'seances#export_tournee_csv'
	get 'export_ics' => 'ics#export_ics'
	get 'export_cc_ics' => 'ics#export_cc_ics'
	get 'export_nodon_ics' => 'ics#export_nodon_ics'
	get 'export_tournee_ics' => 'ics#export_tournee_ics'
	get 'ecranvillage' => 'films#ecranvillage'
	get 'films_a_venir' => 'films#films_a_venir'
	get 'tous_les_films' => 'films#tous_les_films'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
