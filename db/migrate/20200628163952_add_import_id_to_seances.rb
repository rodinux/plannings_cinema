class AddImportIdToSeances < ActiveRecord::Migration[6.0]
  def change
    add_column :seances, :import_id, :integer
  end
end
