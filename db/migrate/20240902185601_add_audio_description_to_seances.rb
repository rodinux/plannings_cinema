class AddAudioDescriptionToSeances < ActiveRecord::Migration[7.0]
  def change
    add_column :seances, :audio_description, :string
  end
end
