class IcsController < ApplicationController
   layout false

  def export_ics
    @seances = Seance.seances_1_mois_avant_apres
    @films = Film.all
  end

  def export_cc_ics
    @seances = Seance.seances_1_mois_avant_apres
    @films = Film.all
  end

  def export_nodon_ics
    @seances = Seance.seances_1_mois_avant_apres
    @films = Film.all
  end

  def export_tournee_ics
    @seances = Seance.seances_1_mois_avant_apres
    @films = Film.all
  end


end