json.array!(@film.seances.seances_1_mois_avant_apres) do |seance|
  json.extract! seance, :id, :film_id, :village_id, :version, :audio_description, :extras, :annulee, :horaire
  json.horaire(seance.horaire.strftime('%s'))
end