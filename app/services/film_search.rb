class FilmSearch
	attr_reader :titrefilm

	def initialize(params)
		params ||= {}
		@titrefilm = parsed_titrefilm(params[:titrefilm])
	end

	def scope
	   Film.where('titrefilm BETWEEN ? and ?')
	end

private

    rescue ArgumentError, TypeError
    	default
end