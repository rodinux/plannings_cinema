class SpecificIframeController < ApplicationController
   layout "layouts/specific_iframe"
   skip_before_action :require_login
   before_action :get_lieu
   after_action :allow_iframe, only: :specific_calendrier_iframe

  def specific_calendrier_iframe
      @seances = Seance.all
  	  @films = Film.all
  	  @villages = Village.all
  	  @date = params[:date] ? Date.parse(params[:date]) : Date.today
      @lieu = "vernoux"
  end

  private

    def get_lieu
      @lieu = params[:lieu]
    end

end