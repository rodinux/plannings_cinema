class AddImportIdToFilms < ActiveRecord::Migration[6.0]
  def change
    add_column :films, :import_id, :integer
  end
end
